package com.example.multimedia.ui.main;

import android.Manifest;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.multimedia.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.IOException;

    public class MainFragment extends Fragment {

        private MainViewModel mViewModel;
        public static MainFragment newInstance() {
            return new MainFragment();
        }

        private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
        private boolean permissionToRecordAccepted = false;
        private String [] permissions = {Manifest.permission.RECORD_AUDIO};

        private ImageView logo;
        private FloatingActionButton record;
        private FloatingActionButton stop;
        private FloatingActionButton play;

        private MediaRecorder mRecorder = null;
        private MediaPlayer mPlayer = null;
        private String fileName;

        public View onCreateView(@NonNull LayoutInflater inflater,
                                 ViewGroup container, Bundle savedInstanceState) {
            mViewModel =
                    new ViewModelProvider(this).get(MainViewModel.class);
            View root = inflater.inflate(R.layout.main_fragment, container, false);

            String ruta = getContext().getExternalFilesDir(null).getAbsolutePath();
            fileName = ruta + "/audiorecord.3gp";

            logo = root.findViewById(R.id.logo);
            record = root.findViewById(R.id.record);
            stop = root.findViewById(R.id.stop);
            play = root.findViewById(R.id.play);
            ActivityCompat.requestPermissions(getActivity(), permissions,
                    REQUEST_RECORD_AUDIO_PERMISSION);

            record.setOnClickListener(v -> {
                logo.setImageDrawable(
                        getResources().getDrawable(R.drawable.recording));
                if (mRecorder == null) {
                    mRecorder = new MediaRecorder();
                    mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
                    mRecorder.setOutputFile(fileName);
                    mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

                    try {
                        mRecorder.prepare();
                    } catch (IOException e) {
                        Log.e("RECORDING",
                                "No es pot iniciar la gravació");
                    }

                    mRecorder.start();
                }
            });

            stop.setOnClickListener(v -> {
                logo.setImageDrawable(
                        getResources().getDrawable(R.drawable.logo));

                if (mRecorder != null) {
                    mRecorder.stop();
                    mRecorder.reset();
                    mRecorder.release();
                    mRecorder = null;
                } else if (mPlayer != null) {
                    mPlayer.stop();
                    mPlayer.release();
                    mPlayer = null;
                }
            });

            play.setOnClickListener(v -> {
                logo.setImageDrawable
                        (getResources().getDrawable(R.drawable.playing));
                if (mRecorder == null && mPlayer == null) {
                    mPlayer = new MediaPlayer();

                    try {
                        mPlayer.setDataSource(fileName);
                        mPlayer.prepare();
                        mPlayer.start();

                        mPlayer.setOnCompletionListener(mediaPlayer -> {
                            stop.callOnClick();
                        });
                    } catch (IOException e) {
                        Log.e("RECORDING", "No es pot iniciar la reproducció");
                    }
                }
            });

            return root;
        }

        @Override
        public void onRequestPermissionsResult(
                int requestCode,
                String[] permissions,
                int[] grantResults) {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            switch (requestCode) {
                case REQUEST_RECORD_AUDIO_PERMISSION:
                    permissionToRecordAccepted = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    break;
            }
            if (!permissionToRecordAccepted) {
                Toast.makeText(
                        getContext(),
                        "Permission needed",
                        Toast.LENGTH_LONG
                ).show();
                //Maybe not so good
                getActivity().finish();
            }
        }
    }